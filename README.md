# Some Random Website with an Image Visualizer

### How to prepare to run
- Install cargo-make, sqlx-cli and wasm-pack:\
https://rustwasm.github.io/wasm-pack/installer/ \
`cargo install --version=0.1.0-beta.1 sqlx-cli` \
`cargo install cargo-make`
- Rename Makefile_example.toml to Makefile.toml
- Rename Rocket_example.toml to Rocket.toml
- Modify the `DATABASE_URL` variable on `Makefile.toml` with whatever database you will want to use (PostgreSQL).
- Modify the `images` and `secret_key` values on `Rocket.toml` with whatever values you want.
- Fill the discord oauth information on `Rocket.toml`
- Run `cargo sqlx database create`

### How to run
- `cargo sqlx migrate run`
- `cargo make run_release`
