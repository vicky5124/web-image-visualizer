use std::fmt::Display;
//use std::convert::TryInto;

use serde::{
    Serialize,
    Deserialize,
};

use wasm_bindgen::{
    prelude::*,
    JsCast,
    JsValue,
};
use wasm_bindgen_futures::JsFuture;

use web_sys::{
    Request,
    RequestInit,
    RequestMode,
    Response,
    //HtmlImageElement,
};

pub type Images = Vec<Image>;

#[derive(Debug, Serialize, Deserialize)]
pub struct Image {
    file_name: String,
    file_size: String,
}

pub fn log(s: impl Display) {
    let value = js_sys::Array::new();
    value.push(&JsValue::from_str(&s.to_string()));
    web_sys::console::log(&value);
}

#[wasm_bindgen(start)]
pub async fn run() -> Result<(), JsValue> {
    log("Running");

    let window = web_sys::window().unwrap();
    let location = window.location();
    let host = location.host()?;

    let api_url = format!("https://{}/api/file_index", host);

    let mut opts = RequestInit::new();
    opts.method("GET");
    opts.mode(RequestMode::Cors);

    let request = Request::new_with_str_and_init(&api_url, &opts)?;

    request
        .headers()
        .set("Accept", "application/vnd.github.v3+json")?;

    let window = web_sys::window().unwrap();
    let resp_value = JsFuture::from(window.fetch_with_request(&request)).await?;

    assert!(resp_value.is_instance_of::<Response>());
    let resp: Response = resp_value.dyn_into().unwrap();

    // Convert this other `Promise` into a rust `Future`.
    let json = JsFuture::from(resp.json()?).await?;

    // Use serde to parse the JSON into a struct.
    let images: Images = json.into_serde().unwrap();

    log(format!("{:#?}", &images));


    // --------------------


    let mut val = 0;

    let image = window.document()
        .unwrap()
        .get_element_by_id("viewer")
        .unwrap();


    let closure = Closure::wrap(Box::new(move |event: web_sys::MouseEvent| {
        if event.button() == 0 {
            let _ = image.set_attribute("src", &format!("https://{}/images/{}", host, images[val].file_name));

            val += 1;
            if val > images.len() - 1 {
                val = 0;
            }
        }
    }) as Box<dyn FnMut(_)>);

    window.add_event_listener_with_callback(
        "mousedown",
        closure.as_ref().unchecked_ref()
    ).expect("what happened?");

    closure.forget();

    Ok(())
}

