use crate::model::*;
use crate::utils::*;

use std::fs;
use std::path::PathBuf;

use rocket::{
    fs::NamedFile,
    get,
    response::{content::Html, Responder},
    State,
};

#[get("/file_index")]
pub async fn file_index(config: &State<LocalConfig>) -> Html<String> {
    let paths = fs::read_dir(&config.images).unwrap();

    let mut files = Vec::new();

    for path in paths {
        let p = path.unwrap();
        let name = &p.file_name();
        let n = name.clone().into_string().unwrap();

        if p.file_type().unwrap().is_file() {
            files.push(n);
        }
    }
    files.sort();

    let mut html = "<h1>Robo Arc Picutes</h1>\n".to_string();
    html += &vector_to_html_images_list(&files);

    Html(html)
}

#[get("/<image>")]
pub async fn images<'a>(image: String, config: &State<LocalConfig>) -> impl Responder<'a, 'static> {
    NamedFile::open(&format!("{}{}", config.images, image))
        .await
        .ok()
}

#[get("/image_visualizer")]
pub async fn image_visualizer<'a>() -> impl Responder<'a, 'static> {
    NamedFile::open("static/index.html").await.ok()
}

#[get("/<path..>")]
pub async fn static_files<'a>(path: PathBuf) -> impl Responder<'a, 'static> {
    NamedFile::open(&format!("static/{}", path.display()))
        .await
        .ok()
}
