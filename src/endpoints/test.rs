use rocket::{fs::NamedFile, get, response::Responder};

#[get("/test")]
pub async fn test<'a>() -> impl Responder<'a, 'static> {
    NamedFile::open("static/python_index.html").await.ok()
}
