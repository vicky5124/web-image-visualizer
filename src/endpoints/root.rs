use crate::model::*;

use rocket::{
    fs::NamedFile,
    get, post,
    response::{content::Html, Responder},
};

#[get("/")]
pub async fn get_index() -> Html<String> {
    Html(format!(
        "
<h1>Welcome!</h1>
<p>Here's all the places you can go to from here:</p>
<h3>Static</h3>
<p><a href=\"/\">Home Page (here)</a></p>
<p><a href=\"/favicon.ico\">Favicon</a></p>
</br>
<h3>Dynamic</h3>
<p><a href=\"/file_index\">File Index</a></p>
<p><a href=\"/image_visualizer\">Image Visualizer</a></p>
<p><a href=\"/python\">Python 3.8 Interpreter (WASM)</a></p>
<p><a href=\"/paste\">Pastebin</a></p>
<p><a href=\"/paste/list\">Pastebins List</a></p>
</br>
<h3>Discord (RoboArc)</h3>
<p>- <a href=\"/discord/\">Login</a></p>
<p>- <a href=\"/discord/user\">User Info</a></p>
</br>
<h3>APIs</h3>
<p>- <a href=\"/api/futured_ai\">Futured AI</a></p>
<p>URL Parameters:</p>
<p>`count`:`u8` (Default 1)</p>
</br>
<p>- <a href=\"/api/jaxtar\">Jaxtar Cute Quotes</a></p>
<p>URL Parameters:</p>
<p>`count`:`u8` (Default 1)</p>
</br>
<p>- <a href=\"/api/uwufy?message=jaxtar is cute\">Text uwufyer</a></p>
<p>URL Parameters:</p>
<p>`message`:`String` (No Default)</p>
</br>
<p>- <a href=\"/api/stats\">Stats (system resources)</a></p>
<p>- <a href=\"/api/file_index\">File Index</a></p>
        ",
    ))
}

#[post("/")]
pub async fn post_index() -> Html<String> {
    Html(format!(
        "
<p>Success!</p>
        ",
    ))
}

#[get("/public_ip")]
pub async fn public_ip(ip: TheIP) -> String {
    format!("{:?}", ip.0)
}

#[get("/python")]
pub async fn python<'a>() -> impl Responder<'a, 'static> {
    NamedFile::open("static/python_index.html").await.ok()
}

#[get("/favicon.ico")]
pub async fn favicon<'a>() -> impl Responder<'a, 'static> {
    NamedFile::open("favicon.png").await.ok()
}
