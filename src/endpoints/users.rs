use crate::model::*;

use std::collections::HashMap;

use sqlx::postgres::PgPool;

use log::*;

use rocket::{
    form::Form,
    get,
    http::{Cookie, CookieJar},
    post,
    request::FlashMessage,
    response::{Flash, Redirect},
    uri, State,
};
use rocket_dyn_templates::Template;

#[post("/login", data = "<login>")]
pub async fn users_login(
    cookies: &CookieJar<'_>,
    login: Form<Login>,
    pool: &State<PgPool>,
) -> Result<Redirect, Flash<Redirect>> {
    info!("Logging in:\n{:#?}", &login);

    let query = match sqlx::query!("SELECT * FROM user_logins WHERE password_hash = encode(digest($1, 'sha256'), 'hex') AND username = $2", login.password, login.username)
        .fetch_optional(pool.inner())
        .await {
            Ok(x) => x,
            Err(why) => {
                error!("There was an error querying the database:\n{}", why);
                return Err(Flash::error(Redirect::to(uri!("/users/", users_login_page)), format!("There was an error querying the database:\n{}", why)));
            },
        };

    if let Some(user) = query {
        cookies.add_private(Cookie::new("user_id", user.id.to_string()));
        Ok(Redirect::to(uri!("/users/", users_index)))
    } else {
        Err(Flash::error(
            Redirect::to(uri!("/users/", users_login_page)),
            "Invalid username/password.",
        ))
    }
}

#[post("/logout")]
pub async fn users_logout(cookies: &CookieJar<'_>) -> Flash<Redirect> {
    cookies.remove_private(Cookie::named("user_id"));
    Flash::success(
        Redirect::to(uri!("/users/", users_login_page)),
        "Successfully logged out.",
    )
}

#[get("/login")]
pub async fn users_login_user(_user: User) -> Redirect {
    Redirect::to(uri!("/users/", users_index))
}

#[get("/login", rank = 2)]
pub async fn users_login_page(flash: Option<FlashMessage<'_>>) -> Template {
    let mut context = HashMap::new();
    if let Some(ref msg) = flash {
        context.insert("flash", msg.message());
        if msg.kind() == "error" {
            context.insert("flash_type", "Error");
        }
    }

    Template::render("login", &context)
}

#[get("/")]
pub async fn users_user_index(user: User) -> Template {
    let mut context = HashMap::new();
    context.insert("user_id", user.0);
    Template::render("index", &context)
}

#[get("/", rank = 2)]
pub async fn users_index() -> Redirect {
    Redirect::to(uri!("/users/", users_login_page))
}
