use rocket::{
    catch,
    http::Status,
    request::Request,
    response::{content::Html, status::Custom},
};

#[catch(404)]
pub async fn not_found(req: &Request<'_>) -> Html<String> {
    Html(format!(
        "
<p>Sorry pal, \"{}\" is not a valid path!</p>
<p><a href=\"javascript:history.back()\">Go Back</a></p>
",
        req.uri()
    ))
}

#[catch(default)]
pub async fn default_catcher(status: Status, req: &Request<'_>) -> Custom<String> {
    let msg = format!(
        "{} - {} ({})",
        status.code,
        status.reason().unwrap(),
        req.uri()
    );
    Custom(status, msg)
}
