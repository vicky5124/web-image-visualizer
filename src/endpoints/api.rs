use crate::model::*;
use crate::utils::*;

use std::{collections::BTreeMap, fs};

use markov::Chain;
use rand::seq::SliceRandom;
use rocket::{
    get,
    http::Status,
    response::{
        content::{
            Html,
            //Content,
            Json,
        },
        Responder,
    },
    State,
};

#[get("/file_index")]
pub async fn file_index_api<'a>(config: &State<LocalConfig>) -> impl Responder<'a, 'static> {
    let paths = fs::read_dir(&config.images).unwrap();

    let mut files = Vec::new();

    for path in paths {
        let p = path.unwrap();
        let name = &p.file_name();
        let n = name.clone().into_string().unwrap();

        if p.file_type().unwrap().is_file() {
            let metadata = &p.metadata();

            let modified = metadata.as_ref().unwrap().modified().unwrap();
            let created = metadata.as_ref().unwrap().created().unwrap();
            let accessed = metadata.as_ref().unwrap().accessed().unwrap();
            let size = metadata.as_ref().unwrap().len();
            let typ = n.split(".").into_iter().last().unwrap().to_string();
            let md5 = get_md5(p, size.clone() as usize).expect("Could not obtain md5sum");

            let utc_mod = time_to_utc(modified);
            let utc_cre = time_to_utc(created);
            let utc_acs = time_to_utc(accessed);

            let mut file_data = BTreeMap::new();
            file_data.insert("accessed_date", utc_acs);
            file_data.insert("created_date", utc_cre);
            file_data.insert("modified_date", utc_mod);
            file_data.insert("file_type", typ);
            file_data.insert("file_size", size.to_string());
            file_data.insert("MD5", md5);
            file_data.insert("file_name", n);

            files.push(file_data);
        }
    }
    files.sort();

    Json(serde_json::to_string(&files).unwrap())
}

#[get("/stats")]
pub async fn stats_api<'a>() -> impl Responder<'a, 'static> {
    let cpu = match sys_info::loadavg() {
        Ok(load) => load,
        Err(_) => return Err(Status::NoContent),
    };

    let memory = match sys_info::mem_info() {
        Ok(memory_info) => memory_info,
        Err(_) => return Err(Status::NoContent),
    };

    let stats = Stats {
        cpu: Cpu {
            five_minutes: format!("{:.2}", cpu.five * 10.0).parse::<f32>().unwrap(),
            fiveteen_minutes: format!("{:.2}", cpu.fifteen * 10.0).parse::<f32>().unwrap(),
            one_minute: format!("{:.2}", cpu.one * 10.0).parse::<f32>().unwrap(),
        },
        mem: Mem {
            ram: Ram {
                available: memory.avail as f32 / 1000.0,
                buffers: memory.buffers as f32 / 1000.0,
                cached: memory.cached as f32 / 1000.0,
                free: memory.free as f32 / 1000.0,
                total: memory.total as f32 / 1000.0,
            },
            swap: Swap {
                used: memory.swap_total as f32 / 1000.0 - memory.swap_free as f32 / 1000.0,
                free: memory.swap_free as f32 / 1000.0,
                total: memory.swap_total as f32 / 1000.0,
            },
        },
    };

    Ok(Json(serde_json::to_string(&stats).unwrap()))
}

#[get("/futured_ai?<count>")]
pub async fn futured_ai<'a>(
    count: Option<u8>,
    data: &State<Chain<String>>,
) -> impl Responder<'a, 'static> {
    let ammount = count.unwrap_or(1);

    let mut file_data = Vec::new();

    for _ in 0..ammount {
        file_data.push(data.generate_str());
    }

    Json(serde_json::to_string(&file_data).unwrap())
}

#[get("/jaxtar?<count>")]
pub async fn jaxtar_api<'a>(
    count: Option<u8>,
    data: &State<CuteQuotes>,
) -> impl Responder<'a, 'static> {
    let ammount = count.unwrap_or(1);
    let mut rng = rand::thread_rng();
    let mut file_data = Vec::new();

    for _ in 0..ammount {
        file_data.push(data.choose(&mut rng).unwrap());
    }

    Json(serde_json::to_string(&file_data).unwrap())
}

#[get("/uwufy?<message>")]
pub async fn uwufy_api(message: String) -> Html<String> {
    let raw_words = message.split(' ');
    let mut words = Vec::new();

    for word in raw_words {
        match word {
            "you" => words.push(word.to_string()),
            "uwu" => words.push(word.to_string()),
            "owo" => words.push(word.to_string()),
            "one" => words.push("wone".to_string()),
            "two" => words.push("two".to_string()),
            "three" => words.push("thwee".to_string()),
            "lewd" => words.push("lewd".to_string()),
            "cute" => words.push("cwute".to_string()),
            _ => {
                if word.len() > 2 {
                    let mut w = word.to_string();
                    w = w.replace("our", "\u{200b}w");

                    w = w.replace("r", "w");
                    w = w.replace("R", "W");

                    w = w.replace("l", "w");
                    w = w.replace("L", "W");

                    w = w.replace("ar", " ");
                    w = w.replace("ai", "+");
                    w = w.replace("a", "wa");
                    w = w.replace("wawa", "waa");
                    w = w.replace(" ", "aw");

                    w = w.replace("ie", " ");
                    w = w.replace("i", "wi");
                    w = w.replace("wiwi", "wii");
                    w = w.replace(" ", "ie");
                    w = w.replace("+", "ai");

                    w = w.replace("ge", " ");
                    w = w.replace("ke", "+");
                    w = w.replace("e", "we");
                    w = w.replace("wewe", "wee");
                    w = w.replace(" ", "ge");
                    w = w.replace("+", "ke");

                    w = w.replace("ou", "=");
                    w = w.replace("cho", " ");
                    w = w.replace("o", "wo");
                    w = w.replace("wowo", "woo");
                    w = w.replace(" ", "cho");

                    w = w.replace("gu", " ");
                    w = w.replace("qu", "+");
                    w = w.replace("u", "wu");
                    w = w.replace("wuwu", "wuu");
                    w = w.replace(" ", "gu");
                    w = w.replace("+", "qu");
                    w = w.replace("=", "ouw");

                    if !word.starts_with("A") {
                        w = w.replace("A", "WA");
                    } else {
                        w = w.replace("A", "Wa");
                    }

                    if !word.starts_with("E") {
                        w = w.replace("E", "WE");
                    } else {
                        w = w.replace("E", "We");
                    }

                    if !word.starts_with("I") {
                        w = w.replace("I", "WI");
                    } else {
                        w = w.replace("I", "Wi");
                    }

                    if !word.starts_with("O") {
                        w = w.replace("O", "WO");
                    } else {
                        w = w.replace("O", "Wo");
                    }

                    if !word.starts_with("U") {
                        w = w.replace("U", "WU");
                    } else {
                        w = w.replace("U", "Wu");
                    }

                    w = w.replace("\u{200b}", "ouw");
                    w = w.replace("@", "@\u{200b}");

                    words.push(w);
                } else {
                    words.push(word.to_string());
                }
            }
        }
    }

    words.push("uwu".to_string());
    let mut m = words.join(" ");

    m = m.replace("ww", "w");
    m = m.replace("Ww", "W");
    m = m.replace("WW", "W");

    Html(format!(
        "
<head>
<meta charset=\"UTF-8\">
<title>UwUfied Text!</title>
<meta name=\"description\" content=\"{0}\">
</head>
<p>{0}</p>
",
        m
    ))
}
