use crate::model::*;
use crate::paste_id::PasteID;
use crate::utils::*;

use std::{collections::HashMap, fs};

use tokio::{fs::File as TokioFile, io::AsyncReadExt, io::AsyncWriteExt};

use syntect::{
    highlighting::{Color, ThemeSet},
    html::highlighted_html_for_string,
    parsing::SyntaxSet,
};

use rocket::{
    form::Form,
    get, post,
    request::FlashMessage,
    response::{content::Html, Flash, Redirect},
};
use rocket_dyn_templates::Template;

#[post("/", data = "<paste>")]
pub async fn upload(paste: Form<PasteData>) -> Result<Redirect, Flash<Redirect>> {
    if paste.code.is_empty() {
        return Err(Flash::error(
            Redirect::to("/paste/"),
            "Can't upload an empty file.",
        ));
    }

    let id = PasteID::new(7);

    let mut file = if paste.extension.is_empty() {
        TokioFile::create(&format!("upload/{}", id)).await.unwrap()
    } else {
        TokioFile::create(&format!("upload/{}.{}", id, paste.extension))
            .await
            .unwrap()
    };

    file.write_all(paste.code.as_bytes()).await.unwrap();

    if paste.extension.is_empty() {
        Ok(Redirect::to(format!("/paste/{}", id)))
    } else {
        Ok(Redirect::to(format!(
            "/paste/{}?lang={}",
            id, paste.extension
        )))
    }
}

#[get("/", rank = 2)]
pub async fn upload_index(flash: Option<FlashMessage<'_>>) -> Template {
    let mut context = HashMap::new();
    if let Some(ref msg) = flash {
        context.insert("flash", msg.message());
        if msg.kind() == "error" {
            context.insert("flash_type", "Error");
        }
    }

    Template::render("paste", &context)
}

#[get("/<id>?<lang>")]
pub async fn retrieve(id: PasteID<'_>, lang: Option<String>) -> Html<String> {
    let filename = if let Some(l) = lang {
        format!("upload/{}.{}", id, l)
    } else {
        format!("upload/{}", id)
    };

    if let Ok(mut file) = TokioFile::open(&filename).await {
        let mut code_text = String::new();
        file.read_to_string(&mut code_text).await.unwrap();

        code_text = format!("\n{}", code_text);

        let ss = SyntaxSet::load_defaults_newlines();
        let ts = ThemeSet::load_defaults();
        let syntax_reference = ss
            .find_syntax_for_file(filename)
            .unwrap()
            .unwrap_or_else(|| ss.find_syntax_plain_text());

        let mut code = String::new();

        let style = "
            pre {
                font-size:13px;
                font-family: Consolas, \"Liberation Mono\", Menlo, Courier, monospace;
            }
            .unselectable {
                -moz-user-select: -moz-none;
                -khtml-user-select: none;
                -webkit-user-select: none;
                -o-user-select: none;
                user-select: none;
            }
        ";

        code += &format!("
            <head>
                <title>{}</title>
                <style>{}</style>
                <link rel=\"stylesheet\"
                href=\"//cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.1.2/build/styles/default.min.css\">
                <script src=\"//cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.1.2/build/highlight.min.js\"></script>
                <script src=\"//cdnjs.cloudflare.com/ajax/libs/highlightjs-line-numbers.js/2.8.0/highlightjs-line-numbers.min.js\">
                </script>
            </head>", id, style);

        let theme = &ts.themes["base16-ocean.dark"];
        let c = theme.settings.background.unwrap_or(Color::WHITE);

        code += &format!(
            "<body style=\"background-color:#{:02x}{:02x}{:02x};\">\n",
            c.r, c.g, c.b
        );

        let html = highlighted_html_for_string(&code_text, &ss, &syntax_reference, theme);
        let html = html.split("\n</span><span style=\"color:#c0c5ce;\">");

        let line_count = html.clone().count() as f64;
        let count = (line_count.log10() + 1.0).floor() as usize;

        let mut lines = Vec::new();

        for (index, i) in html.enumerate() {
            if index == 0 {
                lines.push(i.to_string());
            } else {
                lines.push(format!(
                    "<span unselectable=\"on\" class=\"unselectable\">{:c$} | </span>{}",
                    index,
                    i,
                    c = count
                ));
            }
        }

        let html = lines.join("\n</span><span style=\"color:#c0c5ce;\">");

        code += &format!("{}", html);
        code += &format!("<//body>");

        Html(code)
    } else {
        Html("<pre>Jaxtar is cute</pre>".to_string())
    }
}

#[get("/list")]
pub async fn pastes_list() -> Html<String> {
    let path = "./upload/";
    let paths = fs::read_dir(path).unwrap();

    let mut files = Vec::new();

    for path in paths {
        let p = path.unwrap();
        let name = &p.file_name();
        let n = name.clone().into_string().unwrap();

        if p.file_type().unwrap().is_file() {
            files.push(n);
        }
    }
    files.sort();

    let mut html = "<h1>List of all uploaded Pastes</h1>\n".to_string();
    html += &vector_to_html_pastes(&files);

    Html(html)
}
