use crate::model::*;

use std::{collections::HashMap, sync::Arc};

use rocket::{
    get,
    http::{Cookie, CookieJar, SameSite},
    request::FlashMessage,
    response::{Flash, Redirect},
    uri, State,
};
use rocket_dyn_templates::Template;

#[get("/")]
pub async fn discord_oauth_exists(_user: DiscordUser) -> Redirect {
    Redirect::to(uri!("/discord/user", discord_oauth_redirect))
}

#[get("/", rank = 2)]
pub async fn discord_oauth_redirect(config: &State<LocalConfig>) -> Template {
    let mut context = HashMap::new();
    context.insert("flash", &config.oauth2_url);

    Template::render("discord_login", &context)
}

#[get("/oauth2?<code>")]
pub async fn discord_oauth2(
    code: String,
    config: &State<LocalConfig>,
    cookies: &CookieJar<'_>,
    discord_tokens: &State<DiscordTokens>,
) -> Result<Redirect, Flash<Redirect>> {
    let discord_tokens = Arc::clone(&discord_tokens);

    let client_id = config.client_id;
    let client_secret = config.client_secret.to_string();
    let redirect_uri = config.redirect_uri.to_string();

    let data = OAuthTokenData {
        client_id: client_id as u64,
        client_secret,
        code,
        redirect_uri,
        scope: "identify guilds".to_string(),
        grant_type: "authorization_code".to_string(),
    };

    let client = reqwest::Client::new();
    let resp = if let Ok(x) = client
        .post(&format!("{}/oauth2/token", API_ENDPOINT))
        .form(&data)
        .send()
        .await
        .unwrap()
        .json::<OAuthResponse>()
        .await
    {
        x
    } else {
        println!("wut1");
        return Err(Flash::error(
            Redirect::to(uri!(default)),
            "Error happened while authenticating.",
        ));
    };

    println!("{:#?}", resp);

    //let resp = if let Ok(x) = client.get(&format!("{}/users/@me/guilds", API_ENDPOINT))
    //    .bearer_auth(resp.access_token)
    //    .send()
    //    .await
    //    .unwrap()
    //    .json::<GuildsResponse>()
    //    .await { x } else {
    //        return Html("<p>Discord returned an invalid response.</p>".to_string());
    //    };

    //let client = Client::new(&resp.access_token);

    let user = if let Ok(x) = client
        .get(&format!("{}/users/@me", API_ENDPOINT))
        .bearer_auth(&resp.access_token)
        .send()
        .await
        .unwrap()
        .json::<UserResponse>()
        .await
    {
        x
    } else {
        println!("wut2");
        return Err(Flash::error(
            Redirect::to(uri!(default)),
            "Error happened obtaining user information.",
        ));
    };

    {
        let mut tokens_lock = discord_tokens.lock().await;
        tokens_lock.insert(user.id, resp);
    }

    cookies.add_private(
        Cookie::build("discord_user", user.id.to_string())
            .same_site(SameSite::Lax)
            .finish(),
    );

    Ok(Redirect::to("/discord/user"))
}

#[get("/default")]
pub async fn default(flash: Option<FlashMessage<'_>>) -> Template {
    let mut context = HashMap::new();

    if let Some(ref msg) = flash {
        context.insert("flash", msg.message());
        if msg.kind() == "error" {
            context.insert("flash_type", "Error");
        }
    }

    Template::render("default", &context)
}

#[get("/user")]
pub async fn discord_user_index(
    user: DiscordUser,
    _discord_tokens: &State<DiscordTokens>,
) -> Template {
    //let mut discord_tokens = discord_tokens.lock().await;

    let mut context = HashMap::new();
    context.insert("user_id", user.0);
    Template::render("discord_user", &context)
}

#[get("/user", rank = 2)]
pub async fn discord_not_login_index() -> Redirect {
    Redirect::to(uri!("/discord/", discord_oauth_redirect))
}
