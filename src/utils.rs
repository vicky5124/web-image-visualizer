use std::{
    fmt::Display,
    fs::{self, File},
    io::{self, prelude::*, BufReader},
    time::SystemTime,
};

use chrono::{DateTime, NaiveDateTime, Utc};

pub fn time_to_utc(date: SystemTime) -> String {
    let duration = date.duration_since(std::time::UNIX_EPOCH);
    let chrono_time = NaiveDateTime::from_timestamp(duration.unwrap().as_secs() as i64, 0);
    let utc_time = DateTime::<Utc>::from_utc(chrono_time, Utc);

    format!("{:?}", utc_time)
}

pub fn get_md5(path: fs::DirEntry, size: usize) -> io::Result<String> {
    let p = path.path().to_str().unwrap().to_string();
    let f = File::open(p)?;

    let mut br = BufReader::new(&f);
    let mut buff = vec![0; size];

    br.read_exact(&mut buff)?;

    let digest = md5::compute(buff);
    //let digest = 42069;
    Ok(format!("{:x}", digest))
}

pub fn vector_to_html_images_list(vec: &Vec<impl Display>) -> String {
    let mut html = "<ul>".to_string();
    for i in vec {
        html += &format!("<li><a href=\"/images/{0}\">{0}</a></li>", i);
    }
    html += "</ul>";

    html
}

pub fn vector_to_html_pastes(vec: &Vec<impl Display>) -> String {
    let mut html = "<ul>".to_string();
    for i in vec {
        let i = i.to_string();
        html += &format!(
            "<li><a href=\"/paste/{}\">{}</a></li>",
            {
                let mut file_split = i.split('.');
                if file_split.clone().count() == 1 {
                    i.to_string()
                } else {
                    let filename = file_split.next().unwrap();
                    let extension = file_split.next().unwrap();
                    format!("{}?lang={}", filename, extension)
                }
            },
            i
        );
    }
    html += "</ul>";

    html
}
