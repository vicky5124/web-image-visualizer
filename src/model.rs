use std::sync::Arc;

use serde::{Deserialize, Serialize};
use serde_aux::prelude::*;

//use log::*;

use transient_hashmap::TransientHashMap;

use tokio::sync::Mutex;

use rocket::{
    //config::Config,
    outcome::IntoOutcome,
    request::{FromRequest, Outcome, Request},
    FromForm,
};

pub const API_ENDPOINT: &str = "https://discord.com/api/v8";

pub type CuteQuotes = Vec<String>;

//type DiscordTokens = Arc<Mutex<TransientHashMap<u64, Client>>>;
pub type DiscordTokens = Arc<Mutex<TransientHashMap<u64, OAuthResponse>>>;

#[derive(Serialize, Deserialize, Debug)]
pub struct LocalConfig {
    pub oauth2_url: String,
    pub client_id: u64,
    pub client_secret: String,
    pub redirect_uri: String,
    pub images: String,
}

// ---

#[derive(Debug, Serialize, Deserialize)]
pub struct Stats {
    #[serde(rename = "CPU")]
    pub cpu: Cpu,
    #[serde(rename = "MEM")]
    pub mem: Mem,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Cpu {
    pub five_minutes: f32,
    pub fiveteen_minutes: f32,
    pub one_minute: f32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Mem {
    #[serde(rename = "RAM")]
    pub ram: Ram,
    #[serde(rename = "SWAP")]
    pub swap: Swap,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Ram {
    pub available: f32,
    pub buffers: f32,
    pub cached: f32,
    pub free: f32,
    pub total: f32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Swap {
    pub used: f32,
    pub free: f32,
    pub total: f32,
}

// ---

#[derive(FromForm, Debug)]
pub struct Login {
    pub username: String,
    pub password: String,
    pub login: String,
}

#[derive(FromForm, Debug)]
pub struct PasteData {
    pub code: String,
    pub upload: String,
    pub extension: String,
}

// ---

#[derive(Debug, Serialize, Deserialize)]
pub struct OAuthTokenData {
    pub client_id: u64,
    pub client_secret: String,
    pub grant_type: String,
    pub code: String,
    pub redirect_uri: String,
    pub scope: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct OAuthResponse {
    pub access_token: String,
    pub expires_in: u64,
    pub refresh_token: String,
    pub scope: String,
    pub token_type: String,
}

pub type GuildsResponse = Vec<GuildsResponseElement>;

#[derive(Debug, Serialize, Deserialize)]
pub struct GuildsResponseElement {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub id: u64,
    pub name: String,
    pub icon: Option<String>,
    pub owner: bool,
    pub permissions: String,
    pub features: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserResponse {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub id: u64,
    pub username: String,
    pub avatar: Option<String>,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub discriminator: u16,
    pub public_flags: u64,
    pub flags: u64,
    pub locale: String,
    pub mfa_enabled: bool,
    pub premium_type: Option<u64>,
}

// ---

#[derive(Debug)]
pub struct User(pub usize);

#[derive(Debug)]
pub struct DiscordUser(pub u64);

#[rocket::async_trait]
impl<'a> FromRequest<'a> for User {
    type Error = std::convert::Infallible;

    async fn from_request(request: &'a Request<'_>) -> Outcome<User, Self::Error> {
        request
            .cookies()
            .get_private("user_id")
            .and_then(|cookie| cookie.value().parse().ok())
            .map(|id| User(id))
            .or_forward(())
    }
}

#[rocket::async_trait]
impl<'a> FromRequest<'a> for DiscordUser {
    type Error = std::convert::Infallible;

    async fn from_request(request: &'a Request<'_>) -> Outcome<DiscordUser, Self::Error> {
        request
            .cookies()
            .get_private("discord_user")
            .and_then(|cookie| cookie.value().parse().ok())
            .map(|id| DiscordUser(id))
            .or_forward(())
    }
}

#[derive(Debug)]
pub struct TheIP(pub String);

#[rocket::async_trait]
impl<'a> FromRequest<'a> for TheIP {
    type Error = std::convert::Infallible;

    async fn from_request(request: &'a Request<'_>) -> Outcome<TheIP, Self::Error> {
        let ip = request.client_ip();
        let ip_str = if let Some(ip) = ip {
            ip.to_string()
        } else {
            "localhost".to_string()
        };

        Outcome::Success(TheIP(ip_str))
    }
}
