pub mod endpoints;
pub mod model;
pub mod paste_id;
pub mod utils;

use endpoints::api::*;
use endpoints::discord::*;
use endpoints::error::*;
use endpoints::images::*;
use endpoints::paste::*;
use endpoints::root::*;
use endpoints::test::*;
use endpoints::users::*;
use model::*;

use std::{fs::File, io::prelude::*, sync::Arc};

use sqlx::postgres::PgPoolOptions;

use log::*;

use markov::Chain;
use transient_hashmap::TransientHashMap;

use tokio::sync::Mutex;

use rocket::{
    catchers,
    fairing::AdHoc,
    //config::Config,
    launch,
    routes,
};
use rocket_dyn_templates::Template;

#[launch]
async fn rocket() -> _ {
    let rocket = rocket::build();

    let pool = PgPoolOptions::new()
        .max_connections(4)
        .connect(env!("DATABASE_URL"))
        .await
        .unwrap();

    let futured_dude_chain = {
        let mut file = File::open("futured.json").expect("Could not find file");
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .expect("Could not read file");

        let mut chain = Chain::new();
        let json: Vec<String> = serde_json::from_str(&contents).unwrap();

        for i in json {
            chain.feed_str(&i);
        }

        chain
    };

    let cute_quotes = {
        let mut file = File::open("cute_quotes.txt").expect("Could not find file");
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .expect("Could not read file");

        let mut quotes = contents
            .split("\n")
            .map(|i| i.replace("\\n", "\n"))
            .collect::<Vec<_>>();
        quotes.dedup();
        quotes.remove(quotes.len() - 1);
        //dbg!(&quotes);
        quotes
    };

    let discord_tokens: DiscordTokens = Arc::new(Mutex::new(TransientHashMap::new(604800)));
    let config: LocalConfig = rocket.figment().extract().unwrap();

    trace!("Launching");

    rocket
        .attach(Template::fairing())
        .manage(AdHoc::config::<LocalConfig>())
        .manage(config)
        .manage(pool)
        .manage(futured_dude_chain)
        .manage(cute_quotes)
        .manage(discord_tokens)
        .mount("/", routes![test])
        .mount("/", routes![default])
        .mount("/", routes![python])
        .mount("/", routes![get_index])
        .mount("/", routes![post_index])
        .mount("/", routes![favicon])
        .mount("/", routes![file_index])
        .mount("/", routes![image_visualizer])
        .mount("/", routes![public_ip])
        .mount("/paste", routes![upload])
        .mount("/paste", routes![upload_index])
        .mount("/paste", routes![retrieve])
        .mount("/paste", routes![pastes_list])
        .mount("/images", routes![images])
        .mount("/static", routes![static_files])
        .mount("/api", routes![stats_api])
        .mount("/api", routes![futured_ai])
        .mount("/api", routes![jaxtar_api])
        .mount("/api", routes![file_index_api])
        .mount("/api", routes![uwufy_api])
        .mount("/discord", routes![discord_oauth_redirect])
        .mount("/discord", routes![discord_oauth2])
        .mount("/discord", routes![discord_not_login_index])
        .mount("/discord", routes![discord_user_index])
        .mount("/discord", routes![discord_oauth_exists])
        .mount("/users", routes![users_index])
        .mount("/users", routes![users_user_index])
        .mount("/users", routes![users_login])
        .mount("/users", routes![users_logout])
        .mount("/users", routes![users_login_user])
        .mount("/users", routes![users_login_page])
        .register("/", catchers![not_found, default_catcher])
}
