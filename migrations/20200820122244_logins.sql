-- Add migration script here
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE user_logins(
    id SERIAL NOT NULL,
    username VARCHAR (32) PRIMARY KEY,
    key_code text,
    password_hash text GENERATED ALWAYS AS (encode(digest(key_code, 'sha256'), 'hex')) STORED
);

CREATE INDEX idx_password_hash ON user_logins USING btree (password_hash);
